//
//  FeedCollectionViewController.h
//  PhotoRSSReader
//
//  Created by Pavel on 24.07.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parser.h"
#import "ImageDownloader.h"
#import "DetailViewController.h"
#import <malloc/malloc.h>

@interface FeedCollectionViewController : UICollectionViewController

extern NSString *const URL;

@property (nonatomic, strong) NSMutableArray *authors;
@property (nonatomic, strong) NSMutableArray *titles;
@property (nonatomic, strong) NSMutableArray *thumbnailsLinks;
@property (nonatomic, strong) NSMutableArray *imagesLinks;
@property (nonatomic, strong) NSMutableArray *thumbnails;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end
