//
//  ImageDownloader.m
//  PhotoRSSReader
//
//  Created by Pavel on 24.07.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "ImageDownloader.h"

@implementation ImageDownloader


// Downloading images for feed screen.
- (void)imagesForLinks:(NSArray *)links {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @autoreleasepool {
            self.images = [NSMutableArray arrayWithCapacity:links.count];
            for (int i = 0; i < links.count; i++) {
                [self.images addObject:[NSNull null]];
            }
            for (int i = 0; i < links.count; i++) {
                UIImage *image = [self imageWithImage:[self downloadImageFromURL:links[i]] scaledToSize:CGSizeMake(80, 80)];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //back on main thread
                    [self.images replaceObjectAtIndex:i withObject:image];
                    if (![self.images containsObject:[NSNull null]]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"imagesForLinks"
                                                                            object:self];
                    }
                });
            }
        }
        
    });
}


// Downloading single image from specify URL.
- (UIImage *)downloadImageFromURL:(NSString *)URL {
    @autoreleasepool {
        NSURL *imageURL = [NSURL URLWithString:URL];
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadImageFromURL" object:nil];
        return image;
    }
}


// For memory optimization.
- (UIImage *)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize; {
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


// Singleton implementation.
+ (ImageDownloader *)sharedInstance {
    static dispatch_once_t pred;
    static ImageDownloader *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[ImageDownloader alloc] init];
    });
    return sharedInstance;
}

@end
