//
//  FeedCollectionViewController.m
//  PhotoRSSReader
//
//  Created by Pavel on 24.07.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "FeedCollectionViewController.h"

NSString *const URL = @"http://fotki.yandex.ru/calendar/rss2";


@implementation FeedCollectionViewController

static NSString * const reuseIdentifier = @"Cell";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Push-to-refresh implementation
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(startRefresh)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
    // Arrays primary initialization
    self.thumbnailsLinks = [[NSMutableArray alloc] init];
    self.imagesLinks = [[NSMutableArray alloc] init];
    self.authors = [[NSMutableArray alloc] init];
    self.titles = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(documentParsed)
                                                 name:@"parserDidEndDocument"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(imagesReceived)
                                                 name:@"imagesForLinks"
                                               object:nil];
    
    CacheController *cacheController = [CacheController sharedInstance];
    NSArray *cachedArray = [cacheController getCachedArray];
    if (cachedArray != nil) {
        for (int i = 0; i < [cachedArray count]; i++) {
            [self.authors addObject:[[cachedArray objectAtIndex:i] objectForKey:@"author"]];
            [self.titles addObject:[[cachedArray objectAtIndex:i] objectForKey:@"title"]];
            [self.thumbnailsLinks addObject:[[cachedArray objectAtIndex:i] objectForKey:@"thumbnailLink"]];
            [self.imagesLinks addObject:[[cachedArray objectAtIndex:i] objectForKey:@"imageLink"]];
        }
        self.thumbnails = [NSMutableArray arrayWithCapacity:[self.thumbnailsLinks count]];
        for (int i = 0; i < [self.thumbnailsLinks count]; i++) {
            [self.thumbnails addObject:[NSNull null]];
        }
    } else {
        Parser *parser = [Parser sharedInstance];
        [parser parseFileAtURL:URL];
    }
}


// Action on pull-to-refresh.
- (void)startRefresh {
    Parser *parser = [Parser sharedInstance];
    [parser parseFileAtURL:URL];
}


// Calls when all xml data already parsed.
- (void)documentParsed {
    
    CacheController *cacheController = [CacheController sharedInstance];
    Parser *parser = [Parser sharedInstance];
    [cacheController cacheArray:parser.feed];
    
    for (int i = 0; i < [parser.feed count]; i++) {
        [self.authors addObject:[[parser.feed objectAtIndex:i] objectForKey:@"author"]];
        [self.titles addObject:[[parser.feed objectAtIndex:i] objectForKey:@"title"]];
        [self.thumbnailsLinks addObject:[[parser.feed objectAtIndex:i] objectForKey:@"thumbnailLink"]];
        [self.imagesLinks addObject:[[parser.feed objectAtIndex:i] objectForKey:@"imageLink"]];
    }
    
    self.thumbnails = [NSMutableArray arrayWithCapacity:[self.thumbnailsLinks count]];
    for (int i = 0; i < [self.thumbnailsLinks count]; i++) {
        [self.thumbnails addObject:[NSNull null]];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ImageDownloader *downloader = [ImageDownloader sharedInstance];
        [downloader imagesForLinks:self.thumbnailsLinks];
    });
}


- (void)imagesReceived {
    NSLog(@"RECEIVED");
    CacheController *cacheController = [CacheController sharedInstance];
    [self.refreshControl endRefreshing]; // End refreshing on pull-to-refresh
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
       //background thread code
        ImageDownloader *downloader = [ImageDownloader sharedInstance];
        self.thumbnails = downloader.images;
        for (int i = 0; i < [self.thumbnails count]; i++) {
            [cacheController cacheImage:self.thumbnails[i] forKey:[NSString stringWithFormat:@"%d", i]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            //back on main thread
            [self.collectionView reloadData];
        });
    });
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.thumbnailsLinks count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    CacheController *cacheController = [CacheController sharedInstance];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        if ([self.thumbnails objectAtIndex:indexPath.row] != [NSNull null]) {
            // if thumbnails array has image in indexPath.row position.

            dispatch_async(dispatch_get_main_queue(), ^{
                imageView.image = [self.thumbnails objectAtIndex:indexPath.row];
                [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)]; // Removing activity indicator from collectionView.
            });
            
        } else {
            // if thumbnails array hasn't image in indexPath.row position
            // Try to load image from cache.
            UIImage *cachedImage = [cacheController getCachedImageForKey:[NSString stringWithFormat:@"%ld", (long)indexPath.row]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //back on main thread
                if (cachedImage != nil) {
                    // if cache has right image
                    imageView.image = cachedImage;
                    [self.thumbnails replaceObjectAtIndex:indexPath.row withObject:cachedImage];
                    NSLog(@"Loading From Cache");
                } else {
                    // Loading indicator for cell.
                    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                    self.activityIndicator.frame = cell.bounds;
                    [self.activityIndicator startAnimating];
                    [cell.contentView addSubview:self.activityIndicator];
                    
                    // In case if not all of pictures cached.
                    ImageDownloader *downloader = [ImageDownloader sharedInstance];
                    [downloader imagesForLinks:self.thumbnailsLinks];
                }

            });
        }
    });
    
    [cell addSubview:imageView];
    
    // No more jerky scrolling!
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    return cell;
}


// Setting up size of collection view items.
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100, 100);
}


#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DetailViewController *detailVC = [self detailedViewLayoutAtIndexPath:indexPath];
    
    CacheController *cacheController = [CacheController sharedInstance];
    UIImage *cachedImage = [cacheController getCachedImageForKey:[NSString stringWithFormat:@"%ld_detailed", (long)indexPath.row]];
    
    if (cachedImage != nil) {
        detailVC.imageView.image = cachedImage;
    } else {
        
        // In case if image is not cached and internet connection is unavailable.
        UILabel *alertLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
        alertLabel.text = @"Image not available.\nPlease check the internet connection.";
        alertLabel.numberOfLines = 2;
        alertLabel.textColor = [UIColor whiteColor];
        alertLabel.textAlignment = NSTextAlignmentCenter;
        alertLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
        [detailVC.view addSubview:alertLabel];
        
        // In case if image not cached, it have to be downloaded.
        ImageDownloader *downloader = [ImageDownloader sharedInstance];
        NSString *linkToImage = [self.imagesLinks objectAtIndex:indexPath.row]; // link to detailed image.
        UIImage *detailedImage = [downloader downloadImageFromURL:linkToImage];
        detailVC.imageView.image = detailedImage;
        [cacheController cacheImage:detailVC.imageView.image forKey:[NSString stringWithFormat:@"%ld_detailed", (long)indexPath.row]];
    }
    [detailVC.view addSubview:detailVC.imageView];
    [self.navigationController pushViewController:detailVC animated:YES];
}


- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
    return layoutAttributes;
}

// Setting layout for detailed view controller.
- (DetailViewController *)detailedViewLayoutAtIndexPath:(NSIndexPath *)indexPath {
    
    DetailViewController *detailVC = [[DetailViewController alloc] init];
    detailVC.view = [[UIView alloc] init];
    [detailVC.view setBackgroundColor:[UIColor blackColor]];
    
    // detect the height of our screen
    int height = [UIScreen mainScreen].bounds.size.height;
    
    if (height <= 568) {
        if (height <= 480) { // iPhone 4/4S
            detailVC.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 430, 320, 50)];
            detailVC.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, 320, 300)];
            detailVC.authorLable = [[UILabel alloc] initWithFrame:CGRectMake(14, 8, 300, 20)];
            detailVC.imageTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(14, 25, 300, 20)];
        } else { // iPhone 5/5S
            detailVC.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 508, 320, 60)];
            detailVC.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 130, 320, 300)];
            detailVC.authorLable = [[UILabel alloc] initWithFrame:CGRectMake(16, 10, 300, 20)];
            detailVC.imageTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, 300, 20)];
        }
    } else {
        if (height <= 667) { // iPhone 6
            detailVC.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 600, 375, 67)];
            detailVC.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(28, 150, 320, 300)];
            detailVC.authorLable = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, 300, 20)];
            detailVC.imageTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(30, 30, 300, 20)];
        } else { // iPhone 6 Plus
            detailVC.footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 656, 414, 80)];
            detailVC.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(16, 170, 380, 360)];
            detailVC.authorLable = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, 300, 20)];
            detailVC.imageTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(60, 50, 300, 20)];
        }
    }
    [detailVC.footerView setBackgroundColor:[UIColor whiteColor]];
    [detailVC.footerView setAlpha:0.5];
    
    detailVC.authorLable.text = [self.authors objectAtIndex:indexPath.row];
    detailVC.imageTitleLable.text = [self.titles objectAtIndex:indexPath.row];
    [detailVC.authorLable setFont:[UIFont fontWithName:@"Helvetica" size:16.0]];
    [detailVC.imageTitleLable setFont:[UIFont fontWithName:@"Helvetica" size:16.0]];
    [detailVC.authorLable setTextColor:[UIColor blueColor]];
    [detailVC.imageTitleLable setTextColor:[UIColor blueColor]];
    [detailVC.authorLable setTextAlignment:NSTextAlignmentCenter];
    [detailVC.imageTitleLable setTextAlignment:NSTextAlignmentCenter];
    
    [detailVC.view addSubview:detailVC.footerView];
    [detailVC.view addSubview:detailVC.authorLable];
    [detailVC.footerView addSubview:detailVC.authorLable];
    [detailVC.footerView addSubview:detailVC.imageTitleLable];
    
    return detailVC;
}

@end
