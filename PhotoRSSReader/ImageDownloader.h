//
//  ImageDownloader.h
//  PhotoRSSReader
//
//  Created by Pavel on 24.07.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CacheController.h"

@interface ImageDownloader : NSObject

@property (nonatomic, strong) NSMutableArray *images; // feed images.

- (void)imagesForLinks:(NSArray *)links;
- (UIImage *)downloadImageFromURL:(NSString *)URL;

+ (ImageDownloader *)sharedInstance;

@end
