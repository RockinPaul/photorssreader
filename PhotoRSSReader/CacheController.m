//
//  CacheController.m
//  PhotoRSSReader
//
//  Created by Pavel on 26.07.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import "CacheController.h"

@implementation CacheController


// First of all we need to specify storage path.
- (instancetype)init {
    self = [super init];
    if (self) {
        self.path = NSSearchPathForDirectoriesInDomains(NSPicturesDirectory, NSUserDomainMask, YES);
        self.documentDirectory = [self.path objectAtIndex:0];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        
        // iOS 8 feature: directory can not exists.
        if (![fileManager fileExistsAtPath:self.documentDirectory]) {
            NSLog(@"DOESN'T EXISTS");
            [fileManager createDirectoryAtPath:self.documentDirectory withIntermediateDirectories:NO attributes:nil error:&error];
        }
    }
    return self;
}


// Cache image to internal storage.
- (void)cacheImage:(UIImage *)image forKey:(NSString *)key {
    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *imagePath =[self.documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"image%@.png", key]];
    if (![imageData writeToFile:imagePath atomically:NO]) {
        NSLog((@"Failed to cache image data to disk"));
    } else {
        NSLog(@"the cachedImagedPath is %@", imagePath);
    }
}


// Get cached image from internal storage.
- (UIImage *)getCachedImageForKey:(NSString *)key {
    
    NSString *imagePath = [self.documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"image%@.png", key]];
    UIImage *retrievedImage = [UIImage imageWithContentsOfFile:imagePath];
    return retrievedImage;
}


// Cache array of app data.
- (void)cacheArray:(NSArray *)array {
    NSString *arrayPath = [self.documentDirectory stringByAppendingPathComponent:@"mainInfo"];
    if (![array writeToFile:arrayPath atomically:YES]) {
        NSLog(@"Failed to cache an array");
    } else {
        NSLog(@"The cached array path is %@", arrayPath);
    }
}


// Get array of app data from cache.
- (NSArray *)getCachedArray {
    NSString *arrayPath = [self.documentDirectory stringByAppendingPathComponent:@"mainInfo"];
    NSArray *retrievedArray = [NSArray arrayWithContentsOfFile:arrayPath];
    NSLog(@"%@", arrayPath);
    return retrievedArray;
}


// Singleton implementation.
+ (CacheController *)sharedInstance {
    static dispatch_once_t pred;
    static CacheController *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[CacheController alloc] init];
    });
    return sharedInstance;
}

@end
