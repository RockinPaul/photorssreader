//
//  DetailViewController.h
//  PhotoRSSReader
//
//  Created by Pavel on 24.07.15.
//  Copyright (c) 2015 looi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageDownloader.h"
#import "CacheController.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *authorLable;
@property (nonatomic, strong) UILabel *imageTitleLable;
@property (nonatomic, strong) UIView *footerView;

@end
